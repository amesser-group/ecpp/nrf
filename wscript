#! /usr/bin/env python
# vim: set fileencoding=utf-8 ts=4 sw=4 et



nrf52_chip = {
  'nrf52810_xxaa' : 'nrf52810'
}

def options(opt):
    opt.load('ecpp_nrf',  tooldir = [opt.path.abspath()])

def configure(conf):
    conf.load('ecpp_nrf', tooldir = [conf.path.abspath()])

    if not conf.env['ECPP_BUILDID']:  
      return

    device = conf.env.get_flat('DEVICE')

    f = conf.env.append_value

    nrfx_node = conf.path.find_dir('nordic-nrfx')
    sdk_node  = conf.path.find_dir('nordic-sdk')

    f('INCLUDES', [ 
        conf.path.find_dir('src').abspath(),
        conf.path.find_dir('arm-cmsis').abspath(),

        nrfx_node.abspath(),
        nrfx_node.find_dir('drivers/include').abspath(),
        nrfx_node.find_dir('mdk').abspath(),
        nrfx_node.find_dir('templates').abspath(),
    ])

    inc_nodes = []

    inc_nodes.extend(sdk_node.ant_glob('ble/**/*', src = False, dir = True))
    inc_nodes.extend(sdk_node.ant_glob('external/*', src = False, dir = True))
    inc_nodes.extend(sdk_node.ant_glob('libraries/**/*', src = False, dir = True))

    f('INCLUDES', [x.abspath() for x in inc_nodes])

def build(bld):
    global nrf52_chip

    if not bld.env['ECPP_BUILDID']:
      return

    nrfx_node = bld.path.find_dir('nordic-nrfx')

    chip = nrf52_chip[bld.env.DEVICE]

    source = nrfx_node.ant_glob('drivers/src/**/*.c') + [
             nrfx_node.find_resource('mdk/startup_nrf_common.c'),
             #nrfx_node.find_resource('mdk/gcc_startup_%s.S' % chip),
             nrfx_node.find_resource('mdk/system_%s.c' % chip),
             bld.path.find_resource('src/Heap.cpp')
    ]

    if bld.env.ECPP_NRF_SOFTDEVICE:
      source.append('src/softdevice.c')
      source.extend(bld.path.ant_glob('nordic-softdevice/common/*.c'))
      source.extend(bld.path.ant_glob('nordic-sdk/**/*.c'))

    bld.ecpp_build(
        target   = 'ecpp-nrf',
        source   = source,
        features = 'c cxx cstlib',
    )