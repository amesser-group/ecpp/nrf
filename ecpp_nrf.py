
from waflib.TaskGen   import feature, after_method
from waflib.Configure import conf
from waflib import Utils
import os.path

nrf52_vars = {
  'nrf52810_xxaa' : {
     'CFLAGS'    : ['-mcpu=cortex-m4', '-mthumb', '-mfloat-abi=soft'],
     'CXXFLAGS'  : ['-mcpu=cortex-m4', '-mthumb', '-mfloat-abi=soft'],
     'LINKFLAGS' : ['-mcpu=cortex-m4', '-mthumb'],
  },
}

ecpp_nrf_path = os.path.dirname(__file__)

@conf
def ecpp_setupbuild_platform_nrf(conf, device, board, platform, arch):
    global nrf52_vars

    vars     = nrf52_vars[device]

    envname = 'device_%s' % device

    create = envname not in conf.all_envs
    if create:
        conf.load('ecpp_toolchain ecpp_platform_arm')
        conf.ecpp_setuptoolchain('arm')

        conf.setenv(envname, conf.env)

        conf.env['ECPP_ENVNAME'] = envname

        for k,v in vars.items():
            conf.env.append_value(k, Utils.to_list(v))

        for x in 'CFLAGS CXXFLAGS LINKFLAGS'.split():
            conf.env.append_value(x + "_release", ['-Os'])
            conf.env.append_value(x + "_debug",   ['-O0'])

        conf.env.append_value('DEFINES', ['%s=1' % device.upper()])

        conf.env.append_value('LINKFLAGS', ['-nodefaultlibs', '--static', '-Wl,--gc-sections'])

        # setup linkerscripts
        conf.env['LINKERSCRIPT'] = '%s.ld' % device

        base_dir = conf.root.find_dir(ecpp_nrf_path)

        conf.env.append_value('LIBPATH',  [
            base_dir.find_dir('linkerscripts').abspath(),
            base_dir.find_dir('nordic-nrfx/mdk').abspath(),
        ])

        conf.env['DEVICE'] = device

        # new libc needs ecpp library for support code!
        conf.env['STLIB_c']   = ['c', 'gcc', 'ecpp-nrf']
        conf.env['STLIB_gcc'] = []

        conf.env.append_value('ECPP_FEATURES',['firmware-hex'])
    else:
        conf.setenv(envname)

@conf
def ecpp_nrf_use_softdevice(conf, softdevice):
    device = conf.env.get_flat('DEVICE')

    conf.env['ECPP_NRF_SOFTDEVICE'] = softdevice
    
    conf.env['LINKERSCRIPT'] = '%s_%s.ld' % (device,softdevice)
    
    conf.env.append_value('INCLUDES', [
        conf.root.find_dir(ecpp_nrf_path + os.sep + ('nordic-softdevice/%s_API/include' % softdevice)).abspath(),
        conf.root.find_dir(ecpp_nrf_path + os.sep + ('nordic-softdevice/%s_API/include/nrf52' % softdevice)).abspath(),
        conf.root.find_dir(ecpp_nrf_path + os.sep + ('nordic-softdevice/common')).abspath()
    ])

    conf.env.append_value('DEFINES', [
        'NRF_SOFTDEVICE=%s' % softdevice,
        'MBR_PRESENT=1',
        'SOFTDEVICE_PRESENT=1',
        '%s=1' % softdevice.partition('_')[0].upper()
    ])
    
